module.exports = app => {

    const router = require("express").Router();
    const master = require("../controller/master.ctrl");
    
    const aptCtrl = require("../controller/appointment.ctrl");
    const paymentCtrl = require("../controller/payment.ctrl");
    const calCtrl = require("../controller/calendar.ctrl");

    router.get("/appointment",master.isAuth,aptCtrl.Appointment);
    router.get("/appointment/list",aptCtrl.AppointmentList);
    router.get("/appointment/edit?",aptCtrl.Edit);
    router.get("/appointment/remove?",aptCtrl.Remove);
    router.post("/appointment/save-update/:no",aptCtrl.SaveUpdate);



    router.get("/payments",master.isAuth,paymentCtrl.Payments);
    router.get("/payments/list",paymentCtrl.PaymentsList);
    router.get("/payments/edit?",paymentCtrl.Edit);
    router.post("/payments/save-update/:no",paymentCtrl.SaveUpdate);



    router.get("/calendar",master.isAuth,calCtrl.Calendar);
    router.get("/calendar/data",aptCtrl.AppointmentList);
    router.get("/calendar/edit?",aptCtrl.Edit);
    router.post("/calendar/save-update/:no",aptCtrl.SaveUpdate);




    app.use("/",router);
};