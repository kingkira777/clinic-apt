module.exports = app => {
    const router = require("express").Router();
    const passport = require("passport");
    const loginCtrl = require("../controller/auth.ctrl");

    router.get("/login",loginCtrl.Login);
    router.get("/logout", loginCtrl.Logout);
    router.post("/login", passport.authenticate("local",{
        successRedirect : "/",
        failureRedirect : "/auth/login",
        failureFlash : true
    }),loginCtrl.LoginUser);
 
    app.use("/auth",router);
}