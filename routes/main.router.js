module.exports = app => {

    const router = require("express").Router();
    const master = require("../controller/master.ctrl");

    const dashCtrl = require("../controller/dashboard.ctrl");
    const docCtrl = require("../controller/doctor.ctrl");
    const ptCtrl = require("../controller/patient.ctrl");

    router.get("/",master.isAuth,dashCtrl.Dashboard);
    router.get("/dash/data",dashCtrl.AppointmentData);
    router.get("/dash/count-data",dashCtrl.CountPatientAppointment);


    router.get("/doctors",master.isAuth,docCtrl.List); //VIEW
    router.get("/doctors/list",docCtrl.DoctorList);
    router.get("/doctors/edit?",docCtrl.Edit);
    router.get("/doctors/remove?",docCtrl.Remove);
    router.post("/doctors/save-update/:no",docCtrl.SaveUpdate);
    
    
    
    router.get("/patients",master.isAuth,ptCtrl.List);
    router.get("/patients/list",ptCtrl.PatientList);
    router.get("/patients/edit?",ptCtrl.Edit);
    router.get("/patients/remove?",ptCtrl.Remove);
    router.post("/patients/save-update/:no",ptCtrl.SaveUpdate);




    app.use("/",router);
};