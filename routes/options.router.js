module.exports = app => {

    const router = require("express").Router();
    const master = require("../controller/master.ctrl");
    
    const userCtrl = require("../controller/user.ctrl");

    router.get("/", master.isAuth, userCtrl.UserListView);
    router.get("/profile/:no", master.isAuth, userCtrl.Profile);



    router.get("/list", userCtrl.UserList);
    router.get("/edit?",userCtrl.Edit);
    router.get("/remove?", userCtrl.Remove);

    router.post("/save-update/:no", userCtrl.SaveUpdate);

    app.use("/user",router);
};