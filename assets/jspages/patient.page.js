"use strict";

var Patient = function(){
    var no;

    let _initPatient = () => {
        console.log("Patient init..");


        $("#btnNewPatient").click(()=>{
            no = undefined;
            $("#patient-form").trigger("reset");
        });
        _tablePatient();
    };

    //Remove 
    let _removePatient = () => {
        $("#table-patients").on("click",".btnPtDelete",e=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;

            app.deleteMessage("Remove","Are you sure you want to remove this Patient?", "question",(x)=>{
                if(x){
                    axios.get("/patients/remove?no="+no).then(e=>{
                        console.log(e.data);
                        app.alertMessage("Successfuly Removed", "success");
                        _patientList();
                    });
                }
            });
            return e.preventDefault();
        });
    };

    //Edit
    let _editPatient = () => {
        $("#table-patients").on("click",".btnPtEdit",e=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;

            var form = document.getElementById("patient-form").elements;
            axios.get("/patients/edit?no="+no).then(e=>{
                console.log(e.data);
                var keys = Object.keys(e.data);
                app.setFormData(form,e.data,keys);
            });
            return e.preventDefault();
        });
    };


    //Patient List 
    let _patientList = () => {
        axios.get("/patients/list").then(e=>{
            console.log(e.data);
            _tablePatient(e.data);
        });
    };




    //Save Update 
    let _saveUpdate = () => {
        $("#patient-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById($(e.target).attr("id")).elements;
            var formData = app.serializeForm(form);
            axios.post(url+no,formData).then(e=>{
                console.log(e.data);
                if(e.data.message === "saved"){
                    no = undefined;
                    $("#patient-form").trigger("reset");
                    app.alertMessage("Successfuly Saved", "success");
                }
                if(e.data.message === "updated"){
                    app.alertMessage("Successfuly Updated", "success");
                }
                _patientList();
            });

            return e.preventDefault();
        });
    };


    //Table Patients
    let _tablePatient = (data = []) => {
        $("#table-patients").DataTable({
            data : data,
            columns : [
                {data : null, sTitle : 'Options', render : (e)=>{
                    var htm = ``;
                        htm += `<a data-bs-toggle="modal" data-bs-target=".patient-Modal" role="button" data-no="`+e.no+`" class="btn btn-primary btn-sm btnPtEdit">
                                    <i class="mdi mdi-pencil"></i>
                                </a>
                                <a role="button" data-no="`+e.no+`" class="btn btn-danger btn-sm btnPtDelete">
                                    <i class="mdi mdi-delete"></i>
                                </a>`;
                    return htm;
                }},
                { data : 'ptype', sTitle : "Type"},
                { data : null, sTitle : "Patient",render:(e)=>{
                    return e.lastname+" "+e.firstname+" "+e.middlename;
                }},
                { data : 'dob', sTitle : "DOB"},
                { data : 'gender', sTitle : "Gender"},
                { data : 'weight', sTitle : "Weight"},
                { data : 'contactno', sTitle : "Contact#"},
                { data : 'address', sTitle : "Address"},
            ],
            bDestroy : true
        })
    };


    return {
        initPatient : () => {
            _initPatient();
            _saveUpdate();
            _patientList();
            _editPatient();
            _removePatient();
        }
    }
}();


document.addEventListener("DOMContentLoaded",()=>{
    Patient.initPatient();
}); 
