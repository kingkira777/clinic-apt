"use strict";


var Dashboard = function(){

    let _initDashboard = () => {
        console.log("Dash init..");

    };

    let _countingData = () => {
        axios.get("/dash/count-data").then(e=>{
            // console.log(e.data);
            $("#ptCount").html(e.data.patient);
            $("#aptCount").html(e.data.appointment);
        })
    };

    let _appointmentMonthlyChart = () => {
        axios.get("/dash/data").then(e=>{
            // console.log(e.data);
            _apptChar(e.data);
        });
    };



    let _apptChar = ( _data = []) => {
        const data = {
            labels: [],
            datasets: [{
                label: 'Total Monthly Appointments'+" - "+ new Date().getFullYear(),
                data: _data,
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
                }]
            };
            const config = {
                type: 'line',
                data: data,
                options : {
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                        stacked: true,
                        grid: {
                            display: true,
                            color: "rgba(255,99,132,0.2)"
                        }
                        },
                        x: {
                        grid: {
                            display: false
                        }
                        }
                    }
                }
            };
        new Chart(
            document.getElementById('chart'),
            config,
        );
    };

    return {
        initDash : () => {
            _initDashboard();
            _countingData();
            _appointmentMonthlyChart();
        }
    }
}();




document.addEventListener("DOMContentLoaded",()=>{
    Dashboard.initDash()
});