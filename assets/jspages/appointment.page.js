"use strict";

var Appointments = function(){
    var no;


    let _initAppointment = () => {
        console.log("Appointment init...");


        $("#btnNewApt").click(()=>{

            no = undefined;
            $("#appointment-form").trigger("reset");
        });
        _tableAppointment();
    };

    //Remove
    let _removeApt = () => {
        $("#table-appointments").on("click",".btnAptDelete",(e)=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;
            app.deleteMessage("Remove", "Are you sure you want to remove this Appointment?","question",(x)=>{
                if(x){
                    axios.get("/appointment/remove?no="+no).then(e=>{
                        console.log(e.data);
                        app.alertMessage("Successfuly Deleted", "success");
                        _appointmentList();
                    });
                }
            });
        });
    };

    //Edit 
    let _editApt = () => {
        $("#table-appointments").on("click",".btnAptEdit",(e)=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;
            console.log(no);
            var form = document.getElementById("appointment-form").elements;
            axios.get("/appointment/edit?no="+no).then(e=>{
                var keys = Object.keys(e.data);
                app.setFormData(form,e.data,keys);
            });
        });
    };



    //Appointment List
    let _appointmentList = () => {
        axios.get("/appointment/list").then(e=>{
            console.log(e.data);
            _tableAppointment(e.data);
        });
    };


    //Save update
    let _saveUpdate = () => {
        $("#appointment-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById($(e.target).attr("id")).elements;
            var formData = app.serializeForm(form);

            axios.post(url+no,formData).then(e=>{
                console.log(e.data);
                if(e.data.message === "saved"){
                    no = undefined;
                    $("#appointment-form").trigger("reset");
                    app.alertMessage("Successfuly saved", "success");
                }
                if(e.data.message === "updated"){
                    app.alertMessage("Successfuly updated", "success");
                }
                _appointmentList();
            });

            return e.preventDefault();  
        });
    };

    //Table Appointment = 
    let _tableAppointment = (data= [])=>{
        $("#table-appointments").DataTable({
            data : data,
            columns : [
                {data : null, sTitle : 'Options', render : (e)=>{
                    var htm = ``;
                        htm += `<a data-bs-toggle="modal" data-bs-target=".appointment-Modal" role="button" data-no="`+e.no+`" class="btn btn-primary btn-sm btnAptEdit">
                                    <i class="mdi mdi-pencil"></i>
                                </a>
                                <a role="button" data-no="`+e.no+`" class="btn btn-danger btn-sm btnAptDelete">
                                    <i class="mdi mdi-delete"></i>
                                </a>`;
                    return htm;
                }},
                {data : 'status', sTitle : 'Status'},
                {data : null, sTitle : 'Date/Time', render : (e)=>{
                    return e.sdate+" "+e.stime;
                }},
                {data : null, sTitle : 'Patient', render : (e)=>{
                    return e.ptLastname+" "+e.ptFirstname+" "+e.ptMiddlename;
                }},
                {data : null, sTitle : 'Doctor', render: (e)=>{
                    return e.dcLastname+" "+e.dcFirstname+" "+e.dcMiddlename;
                }},
                {data : 'payment', sTitle : 'Payment Type'},
                {data : 'procedure', sTitle : 'Procedure'},
            ],
            bDestroy :true
        })
    };


    return {
        initAppointment : () => {
            _initAppointment();
            _saveUpdate();
            _appointmentList();
            _editApt();
            _removeApt();
        }
    }

}();


document.addEventListener("DOMContentLoaded",()=>{
    Appointments.initAppointment();
});