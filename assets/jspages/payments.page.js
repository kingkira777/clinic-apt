"use strict";
var Payments = function(){
    var no;
    let _initPayments = () => {
        console.log("Payments init..");


        _tablePayement();
    };


    //Edit
    let _editPayments = () => {
        $("#table-payments").on("click",".btnPaymentEdit",e=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;
            
            var form = document.getElementById("payment-form").elements;
            axios.get("/payments/edit?no="+no).then(e=>{
                console.log(e.data);
                var keys = Object.keys(e.data);
                app.setFormData(form,e.data,keys);
            });
        });
    };


    //Payment List
    let _paymentList = () =>{
        axios.get("/payments/list").then(e=>{
            console.log(e.data);
            _tablePayement(e.data);

        });
    };


    //Save Update
    let _saveUpdate = () => {
        $("#payment-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById($(e.target).attr("id")).elements;
            var formData = app.serializeForm(form);
            axios.post(url+no,formData).then(e=>{
                console.log(e.data);
                
                if(e.data.message === "saved"){
                    app.alertMessage("Successfuly Saved", "success");
                }
                if(e.data.message === "updated"){
                    app.alertMessage("Successfuly Updated", "success");
                }
                _paymentList();
            });
            return e.preventDefault();  
        });
    };


    //TABLE Payments
    let _tablePayement = (data=[])=>{
        $("#table-payments").DataTable({
            data : data,
            columns : [
                {data : null, sTitle : 'Options', render : (e)=>{
                    var htm = ``;
                        htm += `<a data-bs-toggle="modal" data-bs-target=".payment-Modal" role="button" data-no="`+e.no+`" class="btn btn-primary btn-sm btnPaymentEdit">
                                    <i class="mdi mdi-pencil"></i>
                                </a>`;
                    return htm;
                }},
                { data : null, sTitle : 'Appoint Date/Time', render : (e)=>{
                    var htm = ``;
                        htm += e.aptDate+` `+e.aptTime+`<br />`;
                        htm += `<small> Doctor:`+e.dcLastname+` `+e.dcFirstname+` `+e.dcMiddlename+`: `+e.specialty+`</small>`;
                    return htm;
                }},
                { data : null, sTitle : 'Payment Type', render:(e)=>{
                    var htm = ``;
                        htm += e.paymentType+'<br />';
                        htm += `<small>Procedure: `+e.procedure+`</small>`;
                    return htm;
                }},
                { data : null, sTitle : 'Patient', render: (e)=>{
                    return e.ptLastname+" "+e.ptFirstname+" "+e.ptMiddlename;
                }},
                { data : 'amount', sTitle : 'Amount'},
                { data : 'paidDate', sTitle : 'Date Paid'},
            ],
            bDestroy : true
        })
    };

    return {
        initPayment : () => {
            _initPayments();
            _paymentList();
            _saveUpdate();
            _editPayments();
        }
    }
}();

document.addEventListener("DOMContentLoaded",()=>{
    Payments.initPayment();
});