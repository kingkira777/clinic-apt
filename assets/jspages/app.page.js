var app = {

    // userPriv ====================================
    userPriv : () => {
        return {
            access : $("#accessPriv").html(),
            delete : $("#deletePriv").html(),
            edit : $("#editPriv").html()
        }
    },


    // Delete Message============================
    deleteMessage : (title,text,tag,fn,btnText = "Yes, delete it!")=>{
        Swal.fire({
            title : title,
            text : text,
            icon : tag,
            showCancelButton:!0,
            confirmButtonColor:"#34c38f",
            cancelButtonColor:"#f46a6a",
            confirmButtonText:btnText
        }).then(e=>{
            fn(e.isConfirmed);
        });

    },

    // Alert Message ==============================
    alertMessage : (text,tag)=>{
        Swal.fire({
            position:"center",
            icon: tag,
            title: text,
            showConfirmButton: true,
        });
    },


    // Set Form Data========================
    setFormData : (form,data,keys) =>{
        for (var key in form) {
            var item = form[key];
            for (var i in keys) {
                if (item.name === keys[i]) {
                    if (item.type === "checkbox" || item.type === "radio") {
                        item.checked = (data[keys[i]] === "on" || data[keys[i]] === true || data[keys[i]] === "true") ? true : false;
                    } else {
                        item.value = (data[keys[i]]);
                    }
                }
            }

        }
    },

    // SERIALIZE FORM DATA ==================
    serializeForm : (data)=>{
        var formData = {};
        if(data.length == 0 || data == undefined || data == null){
            return formData;
        }else{
            for(var i in data){
                if(data[i].type != undefined){
                // if(data[i].type != "input" && data[i].type != "select")
                    if(data[i].name != "" && data[i].name != undefined){
                        if(data[i].type == "checkbox"){
                            formData[data[i].name] = (data[i].checked === true)? 'true' : 'false';
                        }else if(data[i].type == "radio"){
                            formData[data[i].name] = (data[i].checked === true)? 'true' : 'false';
                        }else{
                            formData[data[i].name] = data[i].value; 
                        }
                    }
                }
            }
            return formData;
        }
    },


    // Search for the exact word in a stirng
    searchString : (str, srcStr) => {
        var rs = str.indexOf(srcStr);
        if(rs >= 0){
            return true;
        }else{
            return false;
        }
    },


    // GET ULR Variable 
    urlString : (pos) => {
        var xurl = window.location.pathname.split("/");
        return xurl[pos];
    },


    // Convert time 24 hrs to 12 hrs
    timeConvert : (_time) => {
        // Check correct time format and split into components
        var time = _time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [_time];
        if (time.length > 1) { // If time format correct
            time = time.slice (1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join (''); // return adjusted time or original string
    },


    //Select 2 Search

    selectSearch : (id,text,modal,url,fn) => {
        $("#"+id+"").select2({
            maximumSelectionLength: 10,
            width : '100%',
            tags: true,
            dropdownParent: $(modal),
            ajax: {
                url: function (params) {
                    var optVal = params.term;
                    var option = new Option(optVal,optVal,true,true);
                    $("#"+id+"").append(option).trigger("change");
                    return url + params.term;
                },
                dataType: 'json',
                processResults: fn,
                cache: true
            },
            placeholder: text,
        });

    },

    // Select Value====
    selectedValue : (id) =>{
        $(".selected-name").on("click",(e)=>{
            var _no = $(e.target).attr("data-no");
            var name = $(e.target).html();
            var opt = new Option(name,_no,true,true);
            $('#'+id+'').append(opt).trigger('change');   
            $(".select2-results").html(""); 
            return  e.preventDefault();
        });
    },



    // Patient Approval List ================
    approvalList : (code="") => {
        var approval = {
            "01" : "Approval w/ Wellness",
            "02" : "Approval w/ no Wellness",
            "03" : "Hold no payment",
            "04" : "Hold for further evaluation",
            "05" : "Disapproved",
            "06" : "No Billing",
            "07" : "No PartB",
            "" : "",
        };
        return approval[code];   
    }
};