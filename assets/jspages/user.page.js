"user strict";


var User = function(){
    var no;
    let _initUser = () => {
        console.log("User Init..");
    }; 


    let _userEditRemove = ()=>{

        $("#table-users").on("click", ".btnUserDelete", (e)=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;

            app.deleteMessage("Delete User", "Are you sure you want to delete this user?","question",(x)=>{
                if(x){
                    axios.get("user/remove?no="+no).then(e=>{
                        console.log(e.data);
                        no = undefined;
                        app.alertMessage("Successfuly updated", "success");
                        _userList();
                    });
                }
            });
        });

        $("#table-users").on("click", ".btnUserEdit", (e)=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;

            var form = document.getElementById("user-form").elements;
            axios.get("user/edit?no="+no).then(e=>{
                console.log(e.data);
                var keys = Object.keys(e.data);
                app.setFormData(form,e.data,keys);
            });
        });

    };


    //User List
    let _userList = () => {
        axios.get("/user/list").then(e=>{
            console.log(e.data);
            _userTable(e.data);
        });
    };

    //SAVE UPDATE PROFILE
    let SaveUpdateProfile = () => {
        $("#profile-form").submit((e)=>{
            var url = $(e.target).attr("action");
            var formId = $(e.target).attr("id");
            var form = document.getElementById(formId).elements;
            var formdata = app.serializeForm(form);

            if(formdata.username === ""){
                app.alertMessage("Username is empty!", "warning");
            }else if(formdata.password == "" || formdata.repassword == ""){
                app.alertMessage("Some password field(s) is empty!", "warning");
            }else if(formdata.password != formdata.repassword){
                app.alertMessage("Password not matched!", "warning");
            }else{
                axios.post(url,formdata).then(e=>{
                    if(e.data.message == "updated"){
                        app.alertMessage("Successfuly updated", "success");
                    }
                });
            }
            return e.preventDefault();
        });
    };




    //SAVE UPDATE USER 
    let _saveUdateUser = () => {
        $("#user-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById("user-form").elements;
            var formdata = app.serializeForm(form);
            
            axios.post(url+no,formdata).then(e=>{
                console.log(e.data);
                if(e.data.message == "saved"){
                    no = undefined;
                    $("#user-form").trigger("reset");
                    app.alertMessage("Successfuly saved", "success");
                }
                if(e.data.message == "updated"){
                    app.alertMessage("Successfuly updated", "success");
                }
                _userList();
            });
            return e.preventDefault();
        });
    };

    //User Table
    let _userTable = (data = [])=> {
        $("#table-users").DataTable({
            data: data,
            columns : [
                {data : null, sTitle : 'Options', render : (e)=>{
                    var htm = ``;
                        htm += `<a data-bs-toggle="modal" data-bs-target=".user-Modal" role="button" data-no="`+e.no+`" class="btn btn-primary btn-sm btnUserEdit">
                                    <i class="mdi mdi-pencil"></i>
                                </a>
                                <a role="button" data-no="`+e.no+`" class="btn btn-danger btn-sm btnUserDelete">
                                    <i class="mdi mdi-delete"></i>
                                </a>`;
                    return htm;
                }},
                {data : 'status', sTitle: "Status"},
                {data : null, sTitle: "User", render:(e)=>{
                    return e.lastname+" "+e.firstname
                }},
                {data : 'dob', sTitle: "DOB"},
            ],
            bDestroy : true
        })
    };


    return{
        initUser : () => {
            _initUser();
            SaveUpdateProfile();
            _saveUdateUser();
            _userList();
            _userTable();
            _userEditRemove();
        }
    }

}();

document.addEventListener("DOMContentLoaded",()=>{
    User.initUser();
});