"use strict";


var Doctor = function(){
    var no;


    let _initDoctor = () => {
        console.log("Doctor init..");

        $("#btnNewDoctor").click(()=>{
            no = undefined;
            $("#doctor-form").trigger("reset");
        });
        _tableDoctor();
    }

    //Remove
    let _removeDoctor = () => {
        $("#table-doctors").on("click",".btnDocDelete",e=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;
            app.deleteMessage("Remove","Are you sure you want to remove this Doctor?","question",(x)=>{
                if(x){
                    axios.get("/doctors/remove?no="+no).then(e=>{
                        // console.log(e.data);
                        app.alertMessage("Doctor successfuly removed","success");
                        _doctorlist();
                    });
                }
            });
            return e.preventDefault();
        });
    };


    //Edit
    let _editDoctor = () => {
        $("#table-doctors").on("click",".btnDocEdit",e=>{
            no = $(e.target).attr("data-no");
            no = (no === undefined)? $(e.target).parents().attr("data-no") : no;

            var form = document.getElementById("doctor-form").elements;

            axios.get("/doctors/edit?no="+no).then(e=>{
                console.log(e.data);
                var keys = Object.keys(e.data);
                app.setFormData(form,e.data,keys);    
            })
            return e.preventDefault();
        });
    };


    //Doctors List
    let _doctorlist = () => {
        axios.get("/doctors/list").then(e=>{
            console.log(e.data);
            _tableDoctor(e.data);
        });
    };

    //Save Update
    let _saveUpdate = () =>{
        $("#doctor-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById($(e.target).attr("id")).elements;
            var formData = app.serializeForm(form);
            axios.post(url+no,formData).then(e=>{
                console.log(e.data);
                if(e.data.message === "saved"){
                    no = undefined;
                    $("#doctor-form").trigger("reset");
                    app.alertMessage("Successfuly Saved!", "success");
                }
                if(e.data.message === "updated"){
                    app.alertMessage("Successfuly Updated!", "success");
                }
                _doctorlist();
            })
            return e.preventDefault();
        });
    };



    //Table Doctors
    let _tableDoctor = (data= [])=>{
        $("#table-doctors").DataTable({
            data : data,
            columns : [ 
                {data : null, sTitle : 'Options', render : (e)=>{
                    var htm = ``;
                        htm += `<a data-bs-toggle="modal" data-bs-target=".doctor-Modal" role="button" data-no="`+e.no+`" class="btn btn-primary btn-sm btnDocEdit">
                                    <i class="mdi mdi-pencil"></i>
                                </a>
                                <a role="button" data-no="`+e.no+`" class="btn btn-danger btn-sm btnDocDelete">
                                    <i class="mdi mdi-delete"></i>
                                </a>`;
                    return htm;
                }},
                {data : 'status', sTitle : "Status"},
                {data : null, sTitle : "Name", render: (e)=>{
                    return e.lastname+" "+e.firstname+" "+e.middlename; 
                }},
                {data : 'dob', sTitle : "DOB"},
                {data : 'specialty', sTitle : "Specialty"},
                {data : 'email', sTitle : "Email"},
                {data : 'email', sTitle : "Contact#"},
            ],
            bDestroy : true
        });

    };

    return {
        initDoctor : () => {
            _initDoctor();
            _saveUpdate();
            _doctorlist();
            _editDoctor();
            _removeDoctor();
        }
    }
}();


document.addEventListener("DOMContentLoaded",()=>{
    Doctor.initDoctor();
});
