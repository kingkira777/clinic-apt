"use strict";

var Calendar = function(){
    var no, calendar;


    let _initCalendar = () => {
        console.log("Calendar init..");

        const dictionary = ['cold', 'wind', 'snow', 'chill'];
        const matrix = ['abcdc','fgwio','chill','pqnsd','uvdxy'];

        var storeArrWord = [];

        var wordHorizontal = '';
        for (var i in matrix){
            
            var HorWords =  _horizontalWord(matrix[i]);
            storeArrWord.push(HorWords);
        }
        console.log(storeArrWord[1][0]);
    };

    let _horizontalWord = (word) =>{
        var words = [];
        var strLength = word.length;
        for(var i=0; i <= strLength; i++ ){
            if(word[i] != undefined){
                words.push(word[i]);
            }
        }
        return words;
    }



    //SAVE UPDATE APPOINTMENT
    let _saveUpdate = () => {
        $("#appointment-form").submit(e=>{
            var url = $(e.target).attr("action");
            var form = document.getElementById($(e.target).attr("id")).elements;
            var formData = app.serializeForm(form);

            
            axios.post(url+no,formData).then(e=>{
                console.log(e.data);
                if(e.data.message === "saved"){
                    no = undefined;
                    $("#appointment-form").trigger("reset");
                    app.alertMessage("Successfuly saved", "success");
                }
                if(e.data.message === "updated"){
                    app.alertMessage("Successfuly updated", "success");
                }
                _calendar();
            });

            return e.preventDefault();  
        });
    }



    //EVENT DATA
    let _eventData = async (data, cb) => {
        var start = moment(data.startStr).format("YYYY-MM-DD");
        var addTwoMonths = moment(start).add(59,'days');
        var end = moment(addTwoMonths).format("YYYY-MM-DD");
        // console.log(data);
        var aptData = await _appointmestData();
        cb(aptData);
        $(".fc-daygrid-day-frame").attr("data-bs-toggle", "modal");
        $(".fc-daygrid-day-frame").attr("data-bs-target", ".appointment-Modal");
    };


    //APPOINTMENT DATA LIST
    let _appointmestData = (start = "",end = "")=>{
        return new Promise(resolve => {
            axios.get("/calendar/data").then(e=>{
                console.log(e.data);
                var data = [], bgColor, TextColor;
                for (var i in e.data){
                    var ptName = e.data[i].ptLastname+" "+e.data[i].ptFirstname+" "+e.data[i].ptMiddlename;
                    var dcName = "Doctor:"+ e.data[i].dcLastname+" "+e.data[i].dcFirstname+" "+e.data[i].dcMiddlename;

                    if(e.data[i].status === "confirmed"){
                        TextColor = "#4deb92";
                        bgColor = "#4deb92";
                    }else{
                        TextColor = "#84968c";
                        bgColor = "#84968c";
                    }

                    var xdata = {
                        id : e.data[i].no,
                        status : e.data[i].status,
                        patient : e.data[i].ptName,
                        doctor : e.data[i].dcName,
                        procedure : e.data[i].procedure,
                        stime : e.data[i].stime,
                        title : ptName+"\n"+dcName,
                        start : e.data[i].sdate+" "+e.data[i].stime,
                        end : e.data[i].sdate+" "+e.data[i].stime,
                        textColor : TextColor,
                        backgroundColor : bgColor,
                    };
                    data.push(xdata);
                }
                resolve(data);
            });
        });
    };


    let _calendar = () =>{
        var calendarEl = document.getElementById("calendar");
        calendar = new FullCalendar.Calendar(calendarEl,{
            initialView: 'dayGridMonth',
            events : _eventData,
            eventDidMount : (data)=>{
                var el = $(data.el);
                el.css("border-color", "black");
                el.css("cursor", "pointer");
            },
            dateClick : (data) => {
                // console.log(data);
                no = undefined;
                $("#appointment-form").trigger("reset");
                $("#sdate").val(data.dateStr);
                $(".fc-daygrid-day-frame").attr("data-bs-toggle", "modal");
                $(".fc-daygrid-day-frame").attr("data-bs-target", ".appointment-Modal");
            },
            eventClick : (event)=>{
                no = event.event._def.publicId;
                var form = document.getElementById("appointment-form").elements;
                axios.get("/calendar/edit?no="+no).then(e=>{
                    console.log(e.data);
                    var keys = Object.keys(e.data);
                    app.setFormData(form,e.data,keys);
                });
            }
        });
        calendar.render();
    }

    return {
        initCalendar : () => {
            _initCalendar();
            _calendar();
            _saveUpdate();
        }
    }
}();




document.addEventListener("DOMContentLoaded",()=>{
    Calendar.initCalendar();
});


