module.exports = (sequelize, Sequelize)=>{

    const doctors = sequelize.define("doctors",{
        no : {
            type : Sequelize.STRING
        },
        status : {
            type : Sequelize.STRING
        },
        firstname : {
            type : Sequelize.STRING
        },
        lastname : {
            type : Sequelize.STRING
        },

        middlename : {
            type : Sequelize.STRING
        },
        dob : {
            type : Sequelize.STRING
        },
        specialty : {
            type : Sequelize.STRING
        },
        email : {
            type : Sequelize.STRING
        },
        contactno : {
            type : Sequelize.STRING
        },
        address : {
            type : Sequelize.STRING
        },
        user : {
            type : Sequelize.STRING
        },
        remove : {
            type : Sequelize.STRING
        },
    },{
        tableName: "doctors"
    });

    return doctors;

}