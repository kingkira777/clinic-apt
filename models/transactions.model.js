module.exports = (sequelize, Sequelize)=>{

    const transactions = sequelize.define("transactions",{
        no : {
            type : Sequelize.STRING
        },
        status : {
            type : Sequelize.STRING
        },
        appointment : {
            type : Sequelize.STRING
        },
        amount : {
            type : Sequelize.STRING
        },
        ptype : {
            type : Sequelize.STRING
        },
        notes : {
            type :Sequelize.STRING
        },
        remove : {
            type : Sequelize.STRING
        },
        user : {
            type : Sequelize.STRING
        },
    },{
        tableName : "transactions"
    });
    return transactions;
};