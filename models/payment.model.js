module.exports = (sequelize, Sequelize)=>{

    const payments = sequelize.define("payments",{
        aptno : {
            type : Sequelize.STRING
        },
        amount : {
            type : Sequelize.STRING
        },
        sdate : {
            type : Sequelize.STRING
        },
        user : {
            type : Sequelize.STRING
        },
        remove : {
            type : Sequelize.STRING
        },
    },{
        tableName : "payments"
    });
    return payments;
};