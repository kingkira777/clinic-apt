module.exports = (sequelize, Sequelize)=>{

    const appointments = sequelize.define("appointments",{
        no : {
            type :Sequelize.STRING
        },
        status : {
            type :Sequelize.STRING
        },
        patient : {
            type :Sequelize.STRING
        },
        doctor : {
            type :Sequelize.STRING
        },
        sdate : {
            type :Sequelize.STRING
        },
        stime : {
            type :Sequelize.STRING
        },
        payment : {
            type :Sequelize.STRING
        },
        procedure : {
            type :Sequelize.STRING
        },
        notes : {
            type :Sequelize.STRING
        },
        remove : {
            type :Sequelize.STRING
        },
        user : {
            type :Sequelize.STRING
        },
    },{
        tableName : "appointments"
    });

    return appointments;
};