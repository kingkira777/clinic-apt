module.exports = (sequelize, Sequelize)=>{

    const patients = sequelize.define("patients",{
        no : {
            type : Sequelize.STRING
        },
        status : {
            type : Sequelize.STRING
        },
        lastname : {
            type : Sequelize.STRING
        },
        firstname : {
            type : Sequelize.STRING
        },
        middlename : {
            type : Sequelize.STRING
        },
        dob : {
            type : Sequelize.STRING
        },
        gender : {
            type : Sequelize.STRING
        },
        email : {
            type : Sequelize.STRING
        },
        contactno : {
            type : Sequelize.STRING
        },
        weight : {
            type : Sequelize.STRING
        },
        ptype : {
            type : Sequelize.STRING
        },
        address : {
            type : Sequelize.TEXT
        },
        remove : {
            type : Sequelize.STRING
        },
        user : {
            type : Sequelize.STRING
        },
    },{
        tableName : "patients"
    });
    return patients;
};