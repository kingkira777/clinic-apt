const Sequelize = require("sequelize");
const sequelize = new Sequelize("clinic","root","",{
    host: "localhost",
    dialect: "mysql",
    operatorsAliases: 0,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});


const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user  = require("./user.model")(sequelize,Sequelize);
db.doctor  = require("./doctor.model")(sequelize,Sequelize);
db.patient  = require("./patients.model")(sequelize,Sequelize);
db.appointment  = require("./appointments.model")(sequelize,Sequelize);
db.transaction  = require("./transactions.model")(sequelize,Sequelize);
db.payment  = require("./payment.model")(sequelize,Sequelize);

module.exports = db;