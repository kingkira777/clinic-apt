module.exports = (sequelize, Sequelize)=>{

    const users = sequelize.define("users",{
        no : {
            type : Sequelize.STRING
        },
        accesslevel : {
            type : Sequelize.STRING
        },
        lastname : {
            type : Sequelize.STRING
        },
        firstname : {
            type : Sequelize.STRING
        },
        dob : {
            type : Sequelize.STRING
        },
        username : {
            type : Sequelize.STRING
        },
        password : {
            type : Sequelize.STRING
        },
        status : {
            type : Sequelize.STRING
        },
        remove : {
            type : Sequelize.STRING
        },
        user : {
            type : Sequelize.STRING
        },
    },{
        tableName : "users"
    })

    return users;
};