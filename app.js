const createError = require('http-errors');
const express = require('express');
const session = require('express-session');
const flash = require('express-flash');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const db = require("./models");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const moment = require("moment");

const app = express();
app.locals.moment = moment;


var sess ={
  secret: "clinic",
  resave: false,
  saveUninitialized: true,
  // cookie : {maxAge : 200000}  //request timeout (in Millisecond)
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// app.use(logger('dev'));
app.use(session(sess));
app.use(flash());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'assets')));
app.use(passport.initialize());
app.use(passport.session());


//Init DB
db.sequelize.sync({force:false});

//PASSPOR AUTH
passport.use(new LocalStrategy(
  async(username,password, cb) =>{
    var data = {
      email : username,
      password : password
    };
    var user = await db.user.findOne({where : {username : data.email, password : data.password}});
    if(user){
      return cb(null,user);
    }else{
      return cb(null,false,{message : `Login Failed`});
    }
  }
));


passport.serializeUser((user,done)=>{
  done(null,user);
});
passport.deserializeUser((id,done)=>{
  done(null,id);
});




require("./routes/auth.router")(app);
require("./routes/main.router")(app);
require("./routes/transactions.router")(app);
require("./routes/options.router")(app);







// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
