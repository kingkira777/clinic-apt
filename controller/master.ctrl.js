
// Check if user is login========================
exports.isAuth = async(req,res,next)=>{
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/auth/login");
};