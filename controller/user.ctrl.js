const db  = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;
const crypto = require("crypto");




//VIEW======================================================
exports.Profile = async(req,res)=>{
    res.render("pages/user/profile",{
        title : "Profile",
        menu : "options",
        user : req.user
    });
    res.end();


};

exports.UserListView = async(req,res)=>{
    res.render("pages/user/user-list",{
        title : "User List",
        menu : "options",
        user : req.user
    });
    res.end();
}


//GET=============================================================
exports.UserList = async(req,res)=>{    
    var userList = await User.findAll({
        where : {
            no : {
                [Op.ne] : req.user.no
            },
            remove : {
                [Op.or] : [
                    {
                        [Op.eq] : null
                    },
                    {
                        [Op.eq] : ''
                    },
                ]
            }
        }
    });
    res.send(userList);
    res.end();
};


exports.Edit = async(req,res)=>{
    var no = req.query.no;
    var editUser = await User.findOne({where : {no : no}});
    res.send(editUser);
    res.end();
};

exports.Remove = async(req,res)=>{
    var no = req.query.no; 
    var data = {
        remove : "true"
    };
    var rUser = await User.update(data,{where : {no : no}});
    if(rUser){
        res.send({message : `removed`});
    }
    res.end();
};


//POST============================================================
exports.SaveUpdate = async(req,res)=>{
    var no = req.params.no, data;

    if(no == "undefined"){
        data = {
            no : crypto.randomBytes(8).toString("hex"),
            accesslevel : req.body.accesslevel,
            status : req.body.status,
            lastname : req.body.lastname,
            firstname : req.body.firstname,
            dob : req.body.dob,
            username : req.body.username,
            password : req.body.password,
            user : req.user.no
        }

        var sUser = await User.create(data);
        if(sUser){
            res.send({message : `saved`});
        }
        res.end();
    }else{
        data = {
            accesslevel : req.body.accesslevel,
            status : req.body.status,
            lastname : req.body.lastname,
            firstname : req.body.firstname,
            dob : req.body.dob,
            username : req.body.username,
            password : req.body.password,
            user : req.user.no
        }

        var sUpdate = await User.update(data,{where : { no : no}});
        if(sUpdate){
            res.send({message : `updated`});
        }
        res.end();
    }

};




