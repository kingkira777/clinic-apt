const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;



//View
exports.Login = async(req,res)=>{   
    res.render("pages/auth/login",{
        title : "Login"
    });
    res.end();
};


//Get
exports.Logout = async(req,res)=>{
    req.logout(function(err) {
        if (err) { return next(err); }
        res.redirect('/');
      });
};


//POST ==================
exports.LoginUser = async(req,res)=>{
    res.redirect('/');
};

