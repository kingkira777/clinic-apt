const db = require("../models");
const Appointment = db.appointment;
const Patient = db.patient;
const Op = db.Sequelize.Op;




// VIEW===================================================
exports.Dashboard = async(req,res)=>{
    res.render("pages/dashboard/dashboard",{
        title : "Dashboard",
        menu : "masterlist",
        user : req.user,
    });
    res.end();
};





//GET=====================================================

//Count Patients And Appointments
exports.CountPatientAppointment = async(req,res)=>{ 
    try {
        var PtTotal = await Patient.findAll({
            where : {
                status : {
                    [Op.eq] : 'active'
                },
                remove : {
                    [Op.eq] : null
                }
            }
        });
    
        var aptTotal = await Appointment.findAll({
            where : {
                remove : {
                    [Op.eq] : null
                } 
            } 
        });
    
        var data = {
            patient : PtTotal.length,
            appointment : aptTotal.length,
        }
        res.send(data);
        res.end();
    } catch (error) {
        res.send({message : error});
        res.end();
    }
};  


//Appointment Data Chart
exports.AppointmentData = async(req,res)=>{
    try {
        var data = {};
        var year = new Date().getFullYear();
        var month = new Date().getMonth() + 1;

        const labels = [
            'January',
            'Febreuary',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];
        var x=0;

        
        for (var i = 1; i <= month; i++){
            var xmonth = (i < 10)? '0'+i : i ;
            var smonth = year+"-"+xmonth;
            data[labels[x]] =  await TotalMonthlyAppointment(smonth);
            x++;
        }

        res.send(data);
        res.end();   
    } catch (error) {
        res.send({message : error});
    }
};

let TotalMonthlyAppointment = async(month) => {
    var aptData = await Appointment.findAll({
        where : {
            sdate :{
                [Op.like] : month+"%"
            }
        }
    });
    return aptData.length;
};



//POST===================================================