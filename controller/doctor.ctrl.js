const db = require("../models");
const Doctor = db.doctor;
const Op = db.Sequelize.Op;
const crypto = require("crypto");



//VIEW ==================================================
exports.List = async(req,res)=>{
    res.render("pages/doctor/list",{
        title : "Doctors",
        menu : "masterlist",
        user : req.user,
    });
    res.end();
};


//GET ==================================================
exports.DoctorList = async(req,res)=>{
    var q = `SELECT * FROM doctors 
            WHERE (remove IS NULL OR remove = '')`;
    var doctorList = await db.sequelize.query(q,{
        model : Doctor
    });
    res.send(doctorList);
    res.end();
};

exports.Edit = async(req,res)=>{
    var no = req.query.no;
    var doctorinfo = await Doctor.findOne({where : {no : no}});
    if(doctorinfo){
        res.send(doctorinfo);
    }
    res.end();
};

exports.Remove = async(req,res)=>{
    var no = req.query.no;
    var data = {
        remove : 'true'
    };
    var rdoc = await Doctor.update(data,{where: {no :no}});
    if(rdoc){
        res.send({message : "removed"});
    }
    res.end();
};




//POST ==================================================
exports.SaveUpdate = async(req,res)=>{
    var no = req.params.no;
    if(no == "undefined"){
        var data = {
            no : crypto.randomBytes(8).toString("hex"),
            status : req.body.status,
            firstname : req.body.firstname,
            lastname : req.body.lastname,
            middlename : req.body.middlename,
            dob : req.body.dob,
            specialty : req.body.specialty,
            email : req.body.email,
            contactno : req.body.contactno,
            address : req.body.address,
            user : "",
        };

        var sdoc = await Doctor.create(data);
        if(sdoc){
            res.send({message : 'saved'});
        }
        res.end();
    }else{
        var data = {
            no : no,
            status : req.body.status,
            firstname : req.body.firstname,
            lastname : req.body.lastname,
            middlename : req.body.middlename,
            dob : req.body.dob,
            specialty : req.body.specialty,
            email : req.body.email,
            contactno : req.body.contactno,
            address : req.body.address,
            user : req.body.user,
        };

        var udoc = await Doctor.update(data, {where : {no : no}});
        if(udoc){
            res.send({message : 'updated'});
        }
        res.end();

    }
};

