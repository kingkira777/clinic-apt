const db = require("../models");
const Patient = db.patient;
const Op = db.Sequelize.Op;
const crypto = require("crypto");



//VIEW================================================================
exports.List = async(req,res)=>{
    res.render("pages/patient/list",{
        title : "Patient List",
        menu : "masterlist",
        user : req.user,
    });
    res.end();
};



//GET================================================================
exports.PatientList = async(req,res)=>{
    var q = `SELECT * FROM patients
            WHERE (remove IS NULL OR remove = '')`;
    var patientList = await db.sequelize.query(q,{
        model : Patient
    });
    res.send(patientList);
    res.end();
};

exports.Edit = async(req,res)=>{
    var no = req.query.no;
    var ePatient = await Patient.findOne({where : {no :no}});
    if(ePatient){
        res.send(ePatient);
    }
    res.end();
};

exports.Remove = async(req,res)=>{
    var no = req.query.no;
    var data = {
        remove : 'true'
    };
    var rPatient = await Patient.update(data,{where : {no : no}});
    if(rPatient){
        res.send({message : 'removed'});
    }
    res.end();
};


//POST================================================================
exports.SaveUpdate = async(req,res)=>{
    var no = req.params.no;
    if(no === "undefined"){
        var data = {
            no : crypto.randomBytes(8).toString("hex"),
            status : req.body.status,
            lastname : req.body.lastname,
            firstname : req.body.firstname,
            middlename : req.body.middlename,
            dob : req.body.dob,
            gender : req.body.gender,
            email : req.body.email,
            contactno : req.body.contactno,
            weight : req.body.weight,
            ptype : req.body.ptype,
            address : req.body.address,
            user : "",
        }

        var sPatient = await Patient.create(data);
        if(sPatient){
            res.send({message : 'saved'});
        }
        res.end();
    }else{
        var data = {
            no : no,
            status : req.body.status,
            lastname : req.body.lastname,
            firstname : req.body.firstname,
            middlename : req.body.middlename,
            dob : req.body.dob,
            gender : req.body.gender,
            email : req.body.email,
            contactno : req.body.contactno,
            weight : req.body.weight,
            ptype : req.body.ptype,
            address : req.body.address,
            user : "",
        }

        var uPatient = await Patient.update(data,{where : {no : no}});
        if(uPatient){
            res.send({message : 'updated'});
        }
        res.end();
    }
}

