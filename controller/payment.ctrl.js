const db = require("../models");
const Appointment = db.appointment;
const Payment = db.payment;
const Op = db.Sequelize.Op;



//VIEW==============================================================================
exports.Payments = async(req,res)=>{

    res.render("pages/payment/list",{
        title : "Payments",
        menu : "transactions",
        user : req.user,
    });
    res.end();

};



//GET==============================================================================
exports.PaymentsList = async(req,res)=>{

    var q = `SELECT 
            a.no,
            a.status,
            a.sdate as aptDate,
            a.stime as aptTime,
            a.payment as paymentType,
            a.procedure,
            a.notes as aptNotes,
            b.amount,
            b.sdate as paidDate,
            c.lastname as ptLastname,
            c.firstname as ptFirstname,
            c.middlename as ptMiddlename,
            c.dob as ptDob,
            d.lastname as dcLastname,
            d.firstname as dcFirstname,
            d.middlename as dcMiddlename,
            d.specialty
            FROM appointments a
            LEFT JOIN payments b on a.no = b.aptno
            LEFT JOIN patients c on a.patient = c.no
            LEFT JOIN doctors d on a.doctor = d.no
            WHERE (a.remove IS NULL OR a.remove = '')`;
    var paymentlist = await db.sequelize.query(q,{
        model : Appointment
    });

    res.send(paymentlist);
    res.end();
};


//EDIT 
exports.Edit = async(req,res)=>{
    var no = req.query.no;
    var editPayment = await Payment.findOne({where : {aptno :no}});
    if(editPayment){
        res.send(editPayment);
    }
    res.end();
};





//POST==============================================================================
exports.SaveUpdate = async(req,res)=>{
    var no = req.params.no;

    var chk = await Payment.findOne({where : {aptno : no}});
    if(chk){
        var data = {
            amount : req.body.amount,
            sdate : req.body.sdate,
            user : ''
        };
        var uPayment = await Payment.update(data,{where : {aptno : no}});
        if(uPayment){
            res.send({message : 'updated'});
        }
        res.end();
    }else{
        var data = {
            aptno : no,
            amount : req.body.amount,
            sdate : req.body.sdate,
            user : ''
        };
        var sPayment = await Payment.create(data);
        if(sPayment){
            res.send({message : 'saved'});
        }
        res.end();
    }

};
