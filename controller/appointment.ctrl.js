const db = require("../models");
const Appointment = db.appointment;
const Doctor = db.doctor;
const Patient = db.patient;
const crypto = require("crypto");




//VIEW======================================================================
exports.Appointment = async(req,res)=>{

    var qp = `SELECT * FROM patients
        WHERE (remove IS NULL OR remove = '')`;
    var qd = `SELECT * FROM doctors
        WHERE (remove IS NULL OR remove = '')`;
    
    var ptList = await db.sequelize.query(qp,{
        model : Patient
    });
    var docList = await db.sequelize.query(qd,{
        model : Doctor
    });


    res.render("pages/appointment/list",{
        title : "Appointment List",
        menu : "transactions",
        user : req.user,
        patients : ptList,
        doctors : docList
    });
    res.end();
};


//GET======================================================================
exports.AppointmentList = async(req,res)=>{
    var q = `SELECT a.*,
            b.lastname as ptLastname,
            b.firstname as ptFirstname,
            b.middlename as ptMiddlename,
            c.lastname as dcLastname,
            c.firstname as dcFirstname,
            c.middlename as dcMiddlename
            FROM appointments a
            LEFT JOIN patients b on a.patient = b.no
            LEFT JOIN doctors c on a.doctor = c.no
            WHERE (a.remove IS NULL OR a.remove = '')`
    var aptlist = await db.sequelize.query(q,{
        model : Appointment
    });
    res.send(aptlist);
    res.end();
};

//EDIT
exports.Edit = async(req,res)=>{
    var no = req.query.no;
    var editapt = await Appointment.findOne({where : {no : no}});
    if(editapt){
        res.send(editapt);
    }
    res.end();
};


//REMOVE
exports.Remove = async(req,res)=>{
    var no = req.query.no;
    var data = {
        remove : 'true'
    };
    var removeapt = await Appointment.update(data,{where : {no : no}});
    if(removeapt){
        res.send({message : 'removed'});
    } 
    res.end();
};



//POST======================================================================
exports.SaveUpdate = async(req,res)=>{
    var no = req.params.no;

    if(no === "undefined"){
        var data = {
            no : crypto.randomBytes(8).toString("hex"),
            status : req.body.status,
            patient : req.body.patient,
            doctor : req.body.doctor,
            sdate : req.body.sdate,
            stime : req.body.stime,
            payment : req.body.payment,
            procedure : req.body.procedure,
            notes : req.body.notes,
            user : ""
        };
        var chk = await _checkForDuplicateDateAndTime(data.sdate,data.stime);
        if(chk){
            res.send({message : 'existing'});
        }else{
            var aptSave = await Appointment.create(data);
            if(aptSave){
                res.send({message : 'saved'});
            }
        }
        res.end();
    }else{
        var data = {
            no : no,
            status : req.body.status,
            patient : req.body.patient,
            doctor : req.body.doctor,
            sdate : req.body.sdate,
            stime : req.body.stime,
            payment : req.body.payment,
            procedure : req.body.procedure,
            notes : req.body.notes,
            user : ""
        };
      
        var aptUpdate = await Appointment.update(data,{where : {no : no}});
        if(aptUpdate){
            res.send({message : 'updated'});
        }
        res.end();

    }
};


let _checkForDuplicateDateAndTime = (date, time) => {
    return new Promise( async resolve => {
        try {
            var chk = await Appointment.findOne({where : {sdate : date, stime :time}});
            resolve(chk);
        } catch (error) {
            console.log(error);
        }
    });
}