const db = require("../models");
const Appointment = db.appointment;
const Doctor = db.doctor;
const Patient = db.patient;





//VIEW=================================================================
exports.Calendar = async(req,res)=>{

    var qp = `SELECT * FROM patients
        WHERE (remove IS NULL OR remove = '')`;
    var qd = `SELECT * FROM doctors
        WHERE (remove IS NULL OR remove = '')`;
    
    var ptList = await db.sequelize.query(qp,{
        model : Patient
    });
    var docList = await db.sequelize.query(qd,{
        model : Doctor
    });


    res.render("pages/calendar/list",{
        title : "Calendar",
        menu : "masterlist",
        user : req.user,
        patients : ptList,
        doctors : docList
    });
    res.end();

};




//GET=================================================================



//POST=================================================================



